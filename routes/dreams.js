var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var router = express.Router();
router.use(bodyParser.json());

// create a schema
var dreamSchema = new Schema({
    _id: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    body: {
        type: String,
        required: true,
        unique: true
    },
    date: {
        type: String,
        unique: true
    },
    lucid: {
        type: String,
        unique: true
    },
    notes: {
        type: String,
        unique: true
    }
}, {
    timestamps: true
});

var Dreams = mongoose.model('Dream', dreamSchema);

router.route('/')
.get(function (req, res, next) {
    Dreams.find({}, function (err, flavor) {
        if (err) throw err;
        res.json(flavor);
    });
})

.post(function (req, res, next) {
    Dreams.create(req.body, function (err, dream) {
        if (err) throw err;
        console.log('Dream created!');
        var id = dream._id;

        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        res.end('Added the dream with id: ' + id);
    });
})

module.exports = router;

