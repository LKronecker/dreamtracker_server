var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var router = express.Router();
router.use(bodyParser.json());

// create a schema
var realityCheckSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    body: {
        type: String,
        required: true,
        unique: true
    },
    image: {
        type: String,
        required: true,
        unique: true
    }
}, {
    timestamps: true
});

var RealityChecks = mongoose.model('RealityCheck', realityCheckSchema);

router.route('/')
.get(function (req, res, next) {
    RealityChecks.find({}, function (err, flavor) {
        if (err) throw err;
        res.json(flavor);
    });
})

module.exports = router;