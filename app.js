//Database
var util = require('util');
var async = require('async');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var database = {
    url: "ds117109.mlab.com:17109",
    name: "dreamtracker",
    user: "lkronecker",
    password: "#!WeAreTheSame_13!#",
};

//Connection
var loginCredentials = database.user + ":" + database.password;
var db = mongoose.connection;

mongoose.connect("mongodb://" + loginCredentials + "@" + database.url + "/" + database.name);

db.on("open", function() {
    console.log("connection to database done from app.js!");
});

db.on("error", function() {
    console.log("error");
});

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');
var dreams = require('./routes/dreams');
var realitychecks = require('./routes/realitychecks');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/dreams', dreams);
app.use('/realitychecks', realitychecks);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
